export class LocalStorageService {
  constructor(key, defaultValue = []) {
    this.key = key;

    if (localStorage.getItem(key) == null) {
      this.setItemLocalStorage(defaultValue);
    }
  }

  setItemLocalStorage(_value) {
    const value = JSON.stringify(_value);
    localStorage.setItem(this.key, value);
  }

  getItemLocalStorage() {
    const _value = localStorage.getItem(this.key);
    return JSON.parse(_value);

  }
}
