import { LocalStorageService } from './localStorage.service';

const _key = "contacts";
let _cache = [];

class ContactsService extends LocalStorageService {
  constructor() {
    super(_key, []);
  }

  async getContacts() {
    _cache = this.getItemLocalStorage(_key)

    return _cache;
  }

  async saveContacts(contact) {
    _cache.push(contact);
    this.setItemLocalStorage(_cache);

    return _cache;
  }

  async removeContact(id) {
    _cache = _cache.filter(c => c.id !== id);
    this.setItemLocalStorage(_cache);

    return _cache;
  }

  async updateContact(contact) {
    _cache = _cache.filter(c => c.id !== contact.id);
    _cache.push(contact);

    this.setItemLocalStorage(_cache);

    return _cache;
  }
}

export default new ContactsService();
