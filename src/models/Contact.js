import { genUuid } from '../utils';

export class Contact {
  constructor({ name = "", phone = "" } = {}) {
    this.id = genUuid();
    this.name = name;
    this.phone = phone;
  }
}
