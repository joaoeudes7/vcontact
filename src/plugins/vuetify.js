import Vue from 'vue';
import Vuetify from 'vuetify';

import 'vuetify/dist/vuetify.min.css';

// Fonts
import 'roboto-fontface/css/roboto/roboto-fontface.css'
// Icons
import 'material-design-icons-iconfont/dist/material-design-icons.css'


Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'md',
  }
});
